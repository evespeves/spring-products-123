package com.citi.training.products.repo;

import java.util.List;

import com.citi.training.products.model.Product;

public interface ProductRepository {

    void save(Product product);

    Product deleteById(int id);
    
    Product findById(int id);

    List<Product> findAll ();
}
